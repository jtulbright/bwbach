import typing
import os
import logging

import requests

logger = logging.getLogger(__name__)


def giphy(search:typing.Optional[str] = None)-> str:
    session = requests.Session()
    with session:
        giphy_key = os.environ.get("GIPHY_API_KEY")
        if giphy_key is None:
            return

        try:
            if search is None:
                response = session.get(f'https://api.giphy.com/v1/gifs/random?api_key={giphy_key}')
                response.raise_for_status()
                data = response.json()
                url = data['data']['images']['original']['url']
            else:
                search.replace(' ', '+')
                response = session.get(f'http://api.giphy.com/v1/gifs/search?q={search}&api_key={giphy_key}&limit=1')
                response.raise_for_status()
                data = response.json()
                url = data['data'][0]['images']['original']['url']
        except Exception as e:
            logger.error(str(e))


    return url



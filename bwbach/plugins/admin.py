import logging

from discord.ext import commands

logger = logging.getLogger(__name__)



class Admin(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def list_commands(self, ctx):
        logger.info("Listing commands.")
        commands = list(self.bot.commands)
        all_commands = "\n".join([str(c) for c in commands])
        await ctx.send(all_commands)
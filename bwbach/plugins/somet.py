import os
import logging
import random

import discord
from discord.ext import commands

from ..giphy import giphy

logger = logging.getLogger(__name__)

class Giphy(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.reply_chance = os.environ.get("GIPHY_REPLY_CHANCE", .01)

    @commands.Cog.listener()
    async def on_message(self, message):
        if message.author == self.bot.user:
            return
        
        roll = random.random()
        logger.debug(f"GIPHY Reply roll: {roll}")

        if roll < float(self.reply_chance):
            logger.debug(f"{roll} is less than {self.reply_chance}")

            word_of_choice = random.choice(message.content.split(" "))
            giphy_url = giphy(word_of_choice)
            embed = discord.Embed(colour=discord.Colour.blue())

            embed.set_image(url =giphy_url)

            await message.channel.send(embed=embed)

        await self.bot.process_commands(message)


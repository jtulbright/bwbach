import logging
import random

import dice
from discord.ext import commands

logger = logging.getLogger(__name__)


good_flavors = [
    "Hooooollyyyyy shiiiiiiiiit",
    "!!!",
    "boom goes the dynamite",
    ":zap::zap::zap:"
]

bad_flavors = [
    "uh oh.",
    "THATS A BIG OLD FUMBONI",
    ":scorpion:"
]

class Dice(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def roll(self, ctx, *, roll:str):
        """see https://pypi.org/project/dice/"""
        try:
            output = dice.roll(roll)
            max = dice.roll_max(roll)
            min = dice.roll_min(roll)

            flavor = ''

            if output == max:
                flavor = random.choice(good_flavors)
            elif output == min:
                flavor = random.choice(bad_flavors)

            author = ctx.author
            
            msg = f"{author.name}: {output} {flavor}"
            await ctx.send(msg)
        except Exception as e:
            logger.error(str(e))
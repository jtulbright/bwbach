import re
import os
import typing
import datetime
import logging

import redis
import discord
from discord.ext import commands

logger = logging.getLogger(__name__)


# Suppress noise about console usage from errors

def _get_demerits(member: discord.Member)->typing.Optional[typing.List[str]]:
    rc = redis.Redis(os.environ.get("REDIS_HOST"))
    key = f"demerit-{member.id}"

    if not rc.exists(key):
        return None
    else:
        if _count_demerits(member=member) == 0:
            return None
        else:
            return rc.hgetall(key)

def _record_demerit(member:discord.Member, reason):
    logger.info(f"Recording a demerit for {member}.")
    rc = redis.Redis(os.environ.get("REDIS_HOST"))
    key = f"demerit-{member.id}"

    now = datetime.datetime.now().isoformat()

    rc.hset(key, reason, now)

def _count_demerits(member:discord.Member):
    rc = redis.Redis(os.environ.get("REDIS_HOST"))
    key = f"demerit-{member.id}"

    return rc.hlen(key)

def _clear_demerits(person):
    pass


class Demerits(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def issue(self, ctx, *, blob:str):
        """<person> a demerit for <reason>.

        Be a force of rightness in the world.
        
        """
        logger.debug("Someone wishes to issue a demerit.")
        match = re.search(r"(.*) a demerit for (.*)", blob)
        if not match:
            await ctx.send("Sorry, I don't quite follow you.")
        else:
            try:
                person, reason = match.groups()
                member = await commands.MemberConverter().convert(ctx, person)
            except Exception as e:
                await ctx.send(f"I'm not sure who {person} is.")
                logger.error(str(e))
            else:
                try:
                    _record_demerit(member, reason)
                except Exception as e:
                    await ctx.send("Dark magic has interferred with the recording of their transgression.")
                    logger.error(str(e))
                else:
                    await ctx.send("Noted.")


    @commands.command()
    async def recite(self, ctx, *, blob:str):
        """the charges against <person>.

        Provides a litany of complaint levied against
        some unfortunate soul.
        """
        logger.debug("Handling recitation request.")
        match = re.search(r"the charges against (.*)", blob)
        if not match:
            await ctx.send("Sorry, I don't quite follow you.")
        else:
            try:
                person = match.groups()[0]
                member = await commands.MemberConverter().convert(ctx, person)
            except Exception as e:
                await ctx.send(f"I'm not sure who {person} is.")
                logger.error(str(e))
            
            else:
                try:
                    if _count_demerits(member) == 0:
                        await ctx.send("This person is blameless.")
                    else:
                        demerits = _get_demerits(member)
                        total_string = []
                        for charge, time in demerits.items():
                            dt = datetime.datetime.fromisoformat(time.decode('utf-8'))
                            year = str(dt.year)
                            month = dt.strftime("%B")
                            day = str(dt.day)
                            this_string = f"On {month} {day}, {year}, {person} was issued a demerit for {charge.decode('utf-8')}."
                            total_string.append(this_string)
                        await ctx.send("\n".join(total_string))
                except Exception as e:
                    logger.error(str(e))



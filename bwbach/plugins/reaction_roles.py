import logging
import typing

import emoji
import discord
from discord.ext import commands

from ..roles import get_member_from_payload, get_role_from_name


logger = logging.getLogger(__name__)

ROLE_CHANNEL_ID = 1015419691897999441

valheim = {"name": "valheim", "emoji":"neck_tail", "role": "vikangz", "match_emoji":"neck_tail"}
blades = {"name": "blades in the dark", "emoji": "dagger", "role":"bitd", "match_emoji":":dagger:"}
delta_green = {"name": "delta green", "emoji":"evergreen_tree", "role":"goons", "match_emoji":":evergreen_tree:"}
ffxiv = {"name":"ffxiv", "emoji":"man_mage", "role":"eorzean", "match_emoji":":man_mage:"}
pendragon = {"name":"pendragon", "emoji":"dragon", "role":"knights-of-camelot", "match_emoji":":dragon:"}
dune = {"name":"dune", "emoji":"desert", "role":"landsraad", "match_emoji":":desert:"}
beamsaber = {"name":"beamsaber", "emoji":"robot", "role":"beamsaber", "match_emoji":":robot:"}
mothership = {"name":"mothership", "emoji":"milky_way", "role":"moms", "match_emoji":":milky_way:"}
one_ring = {"name": "the one ring", "emoji":"ring", "role":"hobbits", "match_emoji":":ring:"}
five_rings = {'name': "legend of the five rings", "emoji":"five", "match_emoji":":keycap_5:", "role":"topaz"}
uo = {'name':"ultima online", "emoji":"violin", "match_emoji":":violin:", "role":"brittanian"}

GUILD_ROLES = [valheim, blades, delta_green, ffxiv, pendragon, dune, beamsaber, mothership, one_ring, five_rings, uo]


class ReactionRoles(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self._generate_message_from_roles()

    @commands.Cog.listener()
    async def on_raw_reaction_add(self, payload: discord.RawReactionActionEvent):

        if payload.channel_id != ROLE_CHANNEL_ID:
            return


        logger.info(f"Someone posted a {payload.emoji} in the roles channel.")

        role_to_adjust = _match_emoji_to_role(payload, self.bot)

        if role_to_adjust is None:
            return
        else:
            member_to_modify = get_member_from_payload(self.bot, payload)
            logger.info(f"Adding role {role_to_adjust.name} to {member_to_modify.name}")
            await member_to_modify.add_roles(role_to_adjust, reason='User adjusted permission in role channel.')
            logger.info("OK")
            # guild_role = discord.utils.get(payload.member.guild.roles, name=role_to_adjust)
            # await member_to_modify.add_roles(guild_role)

    @commands.Cog.listener()
    async def on_raw_reaction_remove(self, payload: discord.RawReactionActionEvent):
        
        if payload.channel_id != ROLE_CHANNEL_ID:
            return


        logger.info(f"Someone removed a {payload.emoji} in the roles channel.")

        role_to_adjust = _match_emoji_to_role(payload, self.bot)

        if role_to_adjust is None:
            return
        else:
            member_to_modify = get_member_from_payload(self.bot, payload)
            
            if role_to_adjust.name not in [r.name for r in member_to_modify.roles]:
                logger.info(f"This person doesnt have the role {role_to_adjust.name} in the first place.")
                return
            else:
                logger.info(f"Removing {role_to_adjust.name} from {member_to_modify.name}")
                await member_to_modify.remove_roles(role_to_adjust, reason="User adjusted permission in role channel")


    def _generate_message_from_roles(self):
        full_message = list()

        for role in GUILD_ROLES:
            message = f":{role['emoji']}: - {role['name']}"
            full_message.append(message)
        
        msg = "\n".join(full_message)
        logger.info(msg)
        return msg



def _match_emoji_to_role(payload, bot) -> typing.Optional[discord.role.Role]:

    payload_emoji = str(emoji.demojize(payload.emoji.name))
    for role_dict in GUILD_ROLES:
        if payload_emoji == role_dict['emoji'] or payload_emoji == role_dict['match_emoji']:
            logger.info("Matched payload emoji to a role.")
            rolename_to_modify = role_dict['role']
            role_to_modify = get_role_from_name(bot, payload, rolename_to_modify)
            if role_to_modify is None:
                logger.info(f"{rolename_to_modify} is not a role on this guild.")
            return role_to_modify
    logger.info(f"Could not match {payload_emoji} to a known role-emoji.")
    return None
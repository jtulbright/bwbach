import logging

import discord
from discord.ext import commands

from .plugins.music import Music
from .plugins.demerits import Demerits
from .plugins.dice import Dice
from .plugins.reaction_roles import ReactionRoles
from .plugins.admin import Admin

logger = logging.getLogger(__name__)


class Bwbach(object):
    def __init__(self, token):
        self._token = token
        intents = discord.Intents().default()
        intents.messages = True
        intents.members = True
        self.bot = commands.Bot(command_prefix=commands.when_mentioned_or("bwbach"), description="A mischievous house spirit.", intents=intents)


    async def register_cogs(self):
        await self.bot.add_cog(Music(self.bot))
        await self.bot.add_cog(Demerits(self.bot))
        await self.bot.add_cog(Dice(self.bot))
        await self.bot.add_cog(Admin(self.bot))
        await self.bot.add_cog(ReactionRoles(self.bot))

    def run(self):
        logger.info(f'Logged in as {self.bot.user}')
        self.bot.run(self._token)




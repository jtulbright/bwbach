import logging
import typing

import discord

logger = logging.getLogger(__name__)

def get_member_from_payload(bot, payload) -> discord.member.Member:
    if payload.member is not None:
        return payload.member
    else:
        logger.info("Payload has no member info. That's ok!")
        if payload.guild_id is not None:
            guild = bot.get_guild(payload.guild_id)
            if payload.user_id is not None:
                member = guild.get_member(payload.user_id)
                return member
            else:
                logger.info("Payload has no user_id")
        else:
            logger.info("Payload has no guild_id")


def get_all_guild_roles_from_payload(bot, payload)->typing.List[discord.role.Role]:
    if payload.guild_id is not None:
        all_roles = bot.get_guild(payload.guild_id).roles
        return all_roles
    return None

def get_role_from_name(bot, payload, rolename:str)->discord.role.Role:
    for role in get_all_guild_roles_from_payload(bot, payload):
        if role.name == rolename:
            return role
    return None
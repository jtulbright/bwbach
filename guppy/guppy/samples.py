
import os

import requests


def get_shakespeare(outpath: str = "shakespeare.txt"):
    file_name = outpath
    if not os.path.isfile(file_name):
        url = "https://raw.githubusercontent.com/karpathy/char-rnn/master/data/tinyshakespeare/input.txt"  # noqa: E501
        data = requests.get(url)
        with open(file_name, "w") as f:
            f.write(data.text)
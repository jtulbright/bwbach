import enum
import re


class DiscordMessage(object):
    def __init__(self, message: str):
        self._message = message
        self._first_line = self._message.split("\n")[0]
        self._other_lines = self._message.split("\n")[1:]
        self._reaction_lines = "\n".join(self._other_lines).split(
            "{Reactions}"
        )[1:]
        self.content = "\n".join(self._other_lines).split("{Reactions}")[0]
        # self.content = re.sub("\n{2,}", "\n", self.content)


class DiscordFile(object):
    NEW_MESSAGE_MARK = re.compile(
        r"\[[0-9]+-[A-z]+-[0-9]+ [0-9]+:[0-9]+ (AM|PM)\] .*\#[0-9]+"
    )
    INVALID_MESSAGES = ["AM", "PM", "\n"]

    def __init__(self, path: str):
        self.path = path

        with open(self.path, "r") as f:
            self.data = f.read()

        self._raw_messages = re.split(DiscordFile.NEW_MESSAGE_MARK, self.data)

        self.messages = [
            DiscordMessage(message)
            for message in self._raw_messages
            if message not in DiscordFile.INVALID_MESSAGES
        ]

        self.text = "".join([m.content for m in self.messages])

    def save(self, outfile: str):
        with open(outfile, "w") as f:
            f.write(self.text)


class Channel(enum.Enum):
    """Provides a mapping between channel names and ids."""

    GENERAL = "311321523250266132"
    TABLETOP = "405860318300864512"
    SANDBOX = "410295284157906957"
    MUSIC = "495472477036609536"
    MOVIE_NIGHT = "698647031597629461"
    DINOSAUR_MEAT_FARM = "520051273278685201"
    FABRIC_FANS = "706362763399659540"
    THE_TOPAZ_CHAMPIONSHIP = "618263090785550357"
    DUNGEONS = "696874976963330098"

import logging
import secrets
import os

from fastapi import Depends, FastAPI, HTTPException, status
from fastapi.security import HTTPBasic, HTTPBasicCredentials
from pydantic import BaseModel

from guppy.discord import Channel
from guppy.guppy import GPTClient

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)



app = FastAPI()

security = HTTPBasic()


def check_auth(credentials: HTTPBasicCredentials = Depends(security)):
    correct_username = secrets.compare_digest(credentials.username, os.environ['GUPPY_USER'])
    correct_password = secrets.compare_digest(credentials.password, os.environ['GUPPY_PWD'])
    if not (correct_username and correct_password):
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect email or password",
            headers={"WWW-Authenticate": "Basic"},
        )
    return True


class GPT2Prompt(BaseModel):
    text: str


logger.info("Creating GPTClient.")
gpt = GPTClient()

logger.info("Starting API.")
app = FastAPI()


@app.get("/")
def index():
    logger.info("Index route hit.")
    return "welcome to index"

@app.get("/hello")
def hello():
    logger.info("Hello route hit.")
    return "hello_world"

@app.get("/secure_hello")
def secure_hello(auth: bool=Depends(check_auth)):
    logger.info("Secure hello route hit.")
    if auth:
        return "secure hello_world"

@app.post("/generate")
def generate(prompt: GPT2Prompt, auth: bool = Depends(check_auth), word_limit: int =50):
    logger.info("Generate route hit.")
    if auth:
        logger.info(f"Generating text for {prompt.text}")
        text = gpt.generate(prompt.text, word_limit=word_limit)
        return text


# @app.get("/finetune")
# def finetune():
#     pass


@app.get("/dump_discord/{channel}")
def dump_discord_channel(channel: Channel, auth: bool = Depends(check_auth)):
    logger.info("Dump discord route hit.")
    if auth:
        gpt.dump_discord_chats(channel)

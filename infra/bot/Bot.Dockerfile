FROM python:alpine3.14

RUN apk add ffmpeg libressl-dev musl-dev libffi-dev gcc cargo python3-dev openssl-dev make

RUN pip install poetry virtualenv
RUN poetry config virtualenvs.create false

RUN mkdir /opt/bwbach
COPY pyproject.toml /opt/bwbach/pyproject.toml
COPY poetry.lock /opt/bwbach/poetry.lock
WORKDIR /opt/bwbach
RUN poetry install --no-root
COPY ./cli.py /opt/bwbach/cli.py
COPY ./bwbach /opt/bwbach/bwbach


RUN poetry install

RUN adduser bwbach_user -D
RUN chown -R bwbach_user:bwbach_user /opt/bwbach
USER bwbach_user

ENTRYPOINT [ "bwbach"]

# pin this
FROM redis:latest
RUN mkdir /etc/redis
COPY ./infra/redis/redis.conf /etc/redis/redis.conf

ENTRYPOINT [ "redis-server" ]
CMD [ "/etc/redis/redis.conf" ]

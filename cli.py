import asyncio
import os
import click
import logging
from logging.config import dictConfig

from bwbach.bot import Bwbach
from bwbach.config import LOGGING_CONFIG

dictConfig(LOGGING_CONFIG)


logger = logging.getLogger(__name__)


@click.command()
def bwbach():
    token = os.environ['DISCORD_TOKEN']
    bb = Bwbach(token)
    asyncio.run(bb.register_cogs())
    bb.run()
    

if __name__ == "__main__":
    print("HI")
    asyncio.run(bwbach())
REGISTRY = registry.gitlab.com/jtulbright
PROJECT = bwbach
VERSION = latest


BOT_IMAGE = $(REGISTRY)/$(PROJECT)/bot:$(VERSION)
REDIS_IMAGE = $(REGISTRY)/$(PROJECT)/redis:v2

NAMESPACE = bwbach

CURRENT_VERSION = $(shell helm ls | grep $(PROJECT) | awk '{print $$10}')

DIRECTORY = ./infra
BOT_DOCKERFILE = $(DIRECTORY)/bot/Bot.Dockerfile
REDIS_DOCKERFILE = $(DIRECTORY)/redis/Redis.Dockerfile

VALUES = $(DIRECTORY)/values.yaml

build:
	docker build -t $(BOT_IMAGE) -f $(BOT_DOCKERFILE) .
	docker build -t $(REDIS_IMAGE) -f $(REDIS_DOCKERFILE) .

push:
	docker push $(BOT_IMAGE)
	docker push $(REDIS_IMAGE)

deploy:
	kubectl config set-context bwbach --cluster=default --namespace=$(NAMESPACE) --user=default
	kubectl config use-context bwbach
	helm install $(PROJECT) --namespace=$(NAMESPACE) $(DIRECTORY) -f $(VALUES)

redeploy:
	sed -i -E "s/version: ([0-9]+)\.([0-9]+)\.0/version: $(CURRENT_VERSION)\.0/" ./infra/Chart.yaml
	sed -i -E "s/appVersion: \"([0-9]+)\.([0-9]+)\"/appVersion: \"$(CURRENT_VERSION)\"/" ./infra/Chart.yaml
	kubectl config set-context bwbach --cluster=default --namespace=$(NAMESPACE) --user=default
	kubectl config use-context bwbach
	helm upgrade -f $(VALUES) --recreate-pods $(PROJECT) $(DIRECTORY)

down:
	helm del $(PROJECT)




